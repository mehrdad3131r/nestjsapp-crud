import { Module } from '@nestjs/common';
import { DogService } from './dog.service';
import { MongooseModule } from '@nestjs/mongoose';
import { DogController } from './dog.controller';
import { Dog, DogSchema } from '../schemas/dog.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Dog.name, schema: DogSchema }])],
  controllers: [DogController],
  providers: [DogService],
})
export class DogModule {}
