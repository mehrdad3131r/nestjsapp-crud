import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DogService } from './dog.service';
import { CreateDogDto } from './dto/create-dog.dto';
import { UpdateDogDto } from './dto/update-dog.dto';

@Controller('dog')
export class DogController {
  constructor(private readonly dogService: DogService) {}

  @Post()
  create(@Body() createDogDto: CreateDogDto) {
    return this.dogService.create(createDogDto);
  }

  @Get()
  findAll() {
    return this.dogService.findAll();
  }

  @Get(':name')
  findOne(@Param('name') name: string) {
    return this.dogService.findOne(name);
  }

  @Patch(':name')
  update(@Param('name') name: string, @Body() updateDogDto: UpdateDogDto) {
    return this.dogService.update(name, updateDogDto);
  }

  @Delete(':name')
  remove(@Param('name') name: string) {
    return this.dogService.remove(name);
  }
}
