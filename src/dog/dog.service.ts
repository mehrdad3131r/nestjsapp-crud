import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Dog, DogDocument } from '../schemas/dog.schema';
import { CreateDogDto } from './dto/create-dog.dto';
import { UpdateDogDto } from './dto/update-dog.dto';

@Injectable()
export class DogService {
  constructor(@InjectModel(Dog.name) private dogModel: Model<DogDocument>) {}
  async create(createDogDto: CreateDogDto): Promise<Dog> {
    return new this.dogModel(createDogDto).save();
  }

  async findAll() {
    return this.dogModel.find();
  }

  async findOne(name: string) {
    return this.dogModel.find({ name });
  }

  async update(name: string, updateDogDto: UpdateDogDto) {
    return this.dogModel.updateOne({ name }, { $set: { ...updateDogDto } });
  }

  async remove(name: string) {
    return this.dogModel.deleteOne({ name });
  }
}
